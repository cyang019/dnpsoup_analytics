# DNP Simulation Examples

> Results generated using dnpsoup, visualized using jupyter notebook.

+ Raw Simulation Data located in [outputs](./outputs)

+ Simulation Input Files Located in [inputs](./inputs)

+ Figures saved in [assets](./assets)

## Simulation Results

[TOPDNP](./assets/TOPDNP/TOPDNP/TOPDNP.md)

[NOVEL](./assets/NOVEL/NOVEL/NOVEL.md)

[MASNOVEL](./assets/MASNOVEL/MASNOVEL/MASNOVEL.md)

[Integrated Solid Effect and Stretched Solid Effect](./assets/ISE_SSE/ISE_SSE/ISE_SSE.md)

[Cross Effect](./assets/CE/CE/CE.md)

