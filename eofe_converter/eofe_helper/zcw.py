import numpy as np
import os


def fibonacci(n):
    if n == 0:
        return 8
    if n == 1:
        return 13
    return fibonacci(n-1) + fibonacci(n-2)

def zcw_from_cnst(m, c1, c2, c3):
    n_m = fibonacci(m+2)
    f_m = fibonacci(m)
    results = []
    for i in range(n_m):
        alpha = 2.0 * np.pi / c3 * np.fmod(i * f_m / n_m, 1.0)
        beta = np.arccos(c1 * (c2 * np.fmod(i/n_m, 1.0) - 1.0))
        results.append([alpha, beta, 0.0])
    return results

def zcw2(n, option):
    if option == 'full':
        return zcw_from_cnst(n, 1, 2, 1)
    elif option == 'hemi':
        return zcw_from_cnst(n, -1, 1, 1)
    elif option == 'octant':
        return zcw_from_cnst(n, -1, 1, 4)
    
def zcw3(n, option, gamma_cnt):
    eulers = zcw2(n, option)
    if gamma_cnt < 2:
        return eulers
    gamma_step = np.pi * 2 / gamma_cnt
    results = []
    for euler in eulers:
        for i in range(gamma_cnt):
            temp = euler[:]
            temp[2] = i * gamma_step
            results.append(temp)
    return results

def getXYZ(r, phi, theta):
    z = r * np.cos(theta)
    xy = r * np.sin(theta)
    x = xy * np.cos(phi)
    y = xy * np.sin(phi)
    return x, y, z
