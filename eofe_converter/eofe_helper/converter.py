import json
import os
from copy import deepcopy
import numpy as np
from scipy.stats import norm, cauchy
from tqdm import tqdm
from .zcw import zcw2, zcw3


def convert_powder_to_intensities(filepath, dest_dir, new_name_prefix):
  with open(filename, 'r', encoding='utf-8') as f:
    data = json.load(f)
  
  euler_scheme = data['settings']['euler_scheme']
  zcw_n = euler_scheme['zcw']
  sphere = euler_scheme['sphere']
  zcw_option = [
    'full', 'hemi', 'octant'
  ][sphere]
  gamma_cnt = None
  if 'gamma_cnt' in euler_scheme:
    gamma_cnt = euler_scheme['gamma_cnt']

  if gamma_cnt is None:
    eulers = zcw2(zcw_n, zcw_option)
  else:
    eulers = zcw3(zcw_n, zcw_option, gamma_cnt)
  
  if os.path.exists(dest_dir):
    print(f'removing content within {dest_dir}...')
    for f in os.listdir(dest_dir):
      os.remove(os.path.join(dest_dir, f))
  else:
    os.makedirs(dest_dir)

  print(f'generating new files in {dest_dir}...')
  for i, euler in enumerate(eulers):
    temp_name = f'{new_name_prefix}_{i:03d}.json'
    temp_path = os.path.join(dest_dir, temp_name)
    temp_data = deepcopy(data)
    temp_data['settings'].pop('euler_scheme', None)
    temp_data['settings']['eulers'] = [
      {
        'alpha': euler[0],
        'beta': euler[1],
        'gamma': euler[2]
      }
    ]
    temp_data['settings']['ncores'] = 1
    with open(temp_path, 'w', encoding='utf-8') as f:
      json.dump(temp_data, f)
  print('finished.')


def convert_scan1d_gammaB1_to_intensities(filepath, dest_dir, new_name_prefix):
  with open(filepath, 'r', encoding='utf-8') as f:
    data = json.load(f)
  
  task_details = data['settings']['task details']
  name = task_details['name']
  spin = task_details['spin']
  values = np.arange(
    task_details['range']['begin'],
    task_details['range']['end'],
    task_details['range']['step'])

  if os.path.exists(dest_dir):
    print(f'removing content within {dest_dir}...')
    for f in os.listdir(dest_dir):
      os.remove(os.path.join(dest_dir, f))
  else:
    os.makedirs(dest_dir)

  print(f'generating new files in {dest_dir}...')
  for i, value in enumerate(values):
    temp_name = f'{new_name_prefix}_{i:03d}.json'
    temp_path = os.path.join(dest_dir, temp_name)
    temp_data = deepcopy(data)
    temp_data['settings'].pop('task details', None)
    temp_data['settings']['task'] = 'PowderIntensity'
    temp_data['pulseseq']['components'][name][spin]['frequency'] = float(value)
    temp_data['settings']['ncores'] = 1
    with open(temp_path, 'w', encoding='utf-8') as f:
      json.dump(temp_data, f)
  print('finished.')

def convert_scan1d_gammaB1_to_intensities_cnst_lvl(filepath, dest_dir, new_name_prefix):
  with open(filepath, 'r', encoding='utf-8') as f:
    data = json.load(f)
  
  task_details = data['settings']['task details']
  name = task_details['name']
  spin = task_details['spin']
  values = np.arange(
    task_details['range']['begin'],
    task_details['range']['end'],
    task_details['range']['step'])

  if os.path.exists(dest_dir):
    print(f'removing content within {dest_dir}...')
    for f in os.listdir(dest_dir):
      os.remove(os.path.join(dest_dir, f))
  else:
    os.makedirs(dest_dir)

  print(f'generating new files in {dest_dir}...')
  for i, value in enumerate(values):
    temp_name = f'{new_name_prefix}_{i:03d}.json'
    temp_path = os.path.join(dest_dir, temp_name)
    temp_data = deepcopy(data)
    temp_data['settings'].pop('task details', None)
    temp_data['settings']['task'] = 'PowderIntensity'
    for comp_name in temp_data['pulseseq']['components']:
      temp_data['pulseseq']['components'][comp_name][spin]['frequency'] = float(value)
    
    for section_name in temp_data['pulseseq']['sections']:
      if temp_data['pulseseq']['sections'][section_name]['type'] == 'Pulse':
        if name not in temp_data['pulseseq']['sections'][section_name]['names']:
          # then it's flip pulse
          sz = int(0.25/(value * temp_data['pulseseq']['increment']))
          sz = max(1, sz)
          temp_data['pulseseq']['sections'][section_name]['size'] = sz

    temp_data['settings']['ncores'] = 1
    with open(temp_path, 'w', encoding='utf-8') as f:
      json.dump(temp_data, f)
  print('finished.')


def convert_scan1d_gammaB1_to_intensities_cnst_lvl_consider_q(filepath, dest_dir, new_name_prefix, q_factor, mw_center_freq, cnt):
  scale = mw_center_freq/q_factor
  lb = mw_center_freq - scale * 3
  ub = mw_center_freq + scale * 3
  mw_freqs = np.linspace(lb, ub, cnt)
  pdf = cauchy.pdf(mw_freqs, loc=mw_center_freq, scale=scale)
  ref_pdf = cauchy.pdf(mw_center_freq, loc=mw_center_freq, scale=scale)

  with open(filepath, 'r', encoding='utf-8') as f:
    data = json.load(f)
  
  task_details = data['settings']['task details']
  name = task_details['name']
  spin = task_details['spin']
  values = np.arange(
    task_details['range']['begin'],
    task_details['range']['end'],
    task_details['range']['step'])

  def gen_intensities_from_actual_val(data, dest_dir, new_name_prefix, desired_vals, pdf, ref_pdf, loc):
    if os.path.exists(dest_dir):
      print(f'removing content within {dest_dir}...')
      for f in os.listdir(dest_dir):
        os.remove(os.path.join(dest_dir, f))
    else:
      os.makedirs(dest_dir)

    print(f'generating new files in {dest_dir}...')
    for i, value in enumerate(desired_vals):
      temp_name = f'{new_name_prefix}_{i:03d}.json'
      temp_path = os.path.join(dest_dir, temp_name)
      temp_data = deepcopy(data)
      temp_data['settings'].pop('task details', None)
      temp_data['settings']['task'] = 'PowderIntensity'
      temp_data['settings']['Gyrotron']['em_frequency'] = mw_freqs[loc]

      actual_value = pdf[loc] / ref_pdf * value
      for comp_name in temp_data['pulseseq']['components']:
        temp_data['pulseseq']['components'][comp_name][spin]['frequency'] = float(actual_value)
      
      for section_name in temp_data['pulseseq']['sections']:
        if temp_data['pulseseq']['sections'][section_name]['type'] == 'Pulse':
          if name not in temp_data['pulseseq']['sections'][section_name]['names']:
            # then it's flip pulse
            sz = int(0.25/(value * temp_data['pulseseq']['increment']))
            sz = max(1, sz)
            temp_data['pulseseq']['sections'][section_name]['size'] = sz

      temp_data['settings']['ncores'] = 1
      with open(temp_path, 'w', encoding='utf-8') as f:
        json.dump(temp_data, f)
    print('finished.')

  new_dirs = []
  for idx in tqdm(range(len(pdf))):
    pdf_val = pdf[idx]
    dest_dir_pdf = f'{dest_dir}_pdf{idx:03d}'
    gen_intensities_from_actual_val(data, dest_dir_pdf, new_name_prefix, values, pdf, ref_pdf, idx)
    new_dirs.append(dest_dir_pdf)
  return new_dirs


def convert_fp_to_intensities(filepath, dest_dir, new_name_prefix):
  with open(filepath, 'r', encoding='utf-8') as f:
    data = json.load(f)
  
  task_details = data['settings']['field range']
  values = np.arange(
    task_details['begin'],
    task_details['end'],
    task_details['step'])

  if os.path.exists(dest_dir):
    print(f'removing content within {dest_dir}...')
    for f in os.listdir(dest_dir):
      os.remove(os.path.join(dest_dir, f))
  else:
    os.makedirs(dest_dir)

  print(f'generating new files in {dest_dir}...')
  for i, value in enumerate(values):
    temp_name = f'{new_name_prefix}_{i:03d}.json'
    temp_path = os.path.join(dest_dir, temp_name)
    temp_data = deepcopy(data)
    temp_data['settings'].pop('field range', None)
    temp_data['settings']['task'] = 'PowderIntensity'
    temp_data['settings']['Magnet']['b0'] = float(value)
    temp_data['settings']['ncores'] = 1
    with open(temp_path, 'w', encoding='utf-8') as f:
      json.dump(temp_data, f)
  print('finished.')


def convert_scan_length_to_intensities(filepath, dest_dir, new_name_prefix):
  with open(filepath, 'r', encoding='utf-8') as f:
    data = json.load(f)
  
  task_details = data['settings']['task details']
  name = task_details['name']
  values = np.arange(
    task_details['range']['begin'],
    task_details['range']['end'],
    task_details['range']['step'])

  if os.path.exists(dest_dir):
    print(f'removing content within {dest_dir}...')
    for f in os.listdir(dest_dir):
      os.remove(os.path.join(dest_dir, f))
  else:
    os.makedirs(dest_dir)

  print(f'generating new files in {dest_dir}...')
  for i, value in enumerate(values):
    temp_name = f'{new_name_prefix}_{i:03d}.json'
    temp_path = os.path.join(dest_dir, temp_name)
    temp_data = deepcopy(data)
    temp_data['settings'].pop('task details', None)
    temp_data['settings']['task'] = 'PowderIntensity'
    temp_data['pulseseq']['sections'][name]['size'] = float(value)
    temp_data['settings']['ncores'] = 1
    with open(temp_path, 'w', encoding='utf-8') as f:
      json.dump(temp_data, f)
  print('finished.')

