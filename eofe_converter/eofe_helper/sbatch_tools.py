import os
from tqdm import tqdm


def gen_sbatch_file(filename, n_tasks, config_name):
  with open(filename, 'w+', encoding='utf-8') as f:
    f.write('#!/bin/bash\n')
    f.write('#SBATCH -p sched_mit_psfc\n')
    f.write(f'#SBATCH --ntasks={n_tasks}\n')
    f.write('#SBATCH --cpus-per-task=1\n')
    f.write('#SBATCH -J CE\n')
    f.write('#SBATCH --export=OMP_NUM_THREADS,ALL\n')
    f.write('module rm gcc/4.8.4\n')
    f.write('module add gcc/8.3.0\n')
    f.write('module load gcc/8.3.0\n')
    f.write('\n# launch the code\n')
    f.write('export OMP_NUM_THREAD=1\n')
    f.write(f'srun --multi-prog {config_name}\n')
  print('finished generating sbatch file.')

def gen_sbatch_config(dest_root, dest_dirname, name_prefix, exe_str=None, common_str=None, dest_parent=None):
  dest_path = os.path.join(dest_root, dest_dirname)
  config_name = f'{dest_dirname}.config'
  n_tasks = len(list(os.listdir(dest_path)))
  dest_config_path = os.path.join(dest_root, config_name)
  if dest_parent is None:
    dest_parent = dest_dirname
  with open(dest_config_path, 'w+') as f:
    for i in tqdm(range(n_tasks)):
      if exe_str is None:
        exe_str = '/home/cyang019/coding/projects/dnpsoup/build/dnpsoup_cli/dnpsoup_exec'
      if common_str is None:
        common_str = f'/home/cyang019/workspace/NOVEL/tasks/{dest_parent}'
      file_part = f'{common_str}/{dest_dirname}/{name_prefix}_{i:03d}.json'
      output_part = f'{common_str}/{dest_dirname}_results/{name_prefix}_{i:03d}.result'
      line = f'{i} {exe_str} {file_part} {output_part}\n'
      f.write(line)
  print('finished generating sbatch config file.')
