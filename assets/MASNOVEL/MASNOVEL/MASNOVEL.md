```python
import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
```


```python
%matplotlib inline
```


```python
os.getcwd()
```




    '/home/chen/coding/projects/dnpsoup/dnpsoup_analytics'




```python
def get_data(filename):
    df = pd.read_csv(filename, skiprows=2, header=None)
    return df
```


```python
data_dir = 'outputs/NOVEL-MAS/'
```


```python
if not os.path.exists('assets/MASNOVEL'):
    os.makedirs('assets/MASNOVEL')
```

## BuildUp

+ Powder Options


```python
zcws = [
    'zcw34',    # took 46.718 seconds
    'zcw55',    # took 75.383 seconds
    'zcw89',    # took 118.866 seconds
    'zcw144',   # took 194.516 seconds
    'zcw233',   # took 314.671 seconds
    'zcw377',   # took 517.494 seconds
]
filenames = [
    f'eH_NOVEL_xband_loop10_8kHz_inc40ns_{zcw}_buildup.result' for zcw in zcws
]
n = len(filenames)
colors = plt.cm.rainbow(np.linspace(0.05, 0.95, n))
markers = [
    'x', '+', 'o', 's', '^', 'D', 'p', 'h', 'v'
]

fig, axes = plt.subplots(3, 1, figsize=(10, 15), dpi=100)
for filename, color, m, label in zip(filenames, colors, markers, zcws):
    filepath = os.path.join(data_dir, filename)
    df = get_data(filepath)
    axes[0].plot(df[0]*1e3, -df[1], color=color, label=label, linewidth=2)
    axes[1].plot(df[0]*1e3, -df[1], color=color, label=label, linewidth=1)
    axes[2].plot(df[0]*1e3, -df[1], linestyle='solid', 
                 color=color, label=label, markerfacecolor='none')

axes[1].set_xlim(1.52, 1.705)
axes[1].set_ylim(61.5, 62.6)

axes[2].set_xlabel('ms')
axes[2].set_xlim(0, 0.03)
axes[2].set_ylim(5, 7)

axes[0].legend()
axes[1].legend(loc='best')
axes[2].legend()
plt.savefig('assets/MASNOVEL/eH_xband_NOVEL-MAS_zcw_options.ps')
plt.savefig('assets/MASNOVEL/eH_xband_NOVEL-MAS_zcw_options.png')
plt.show()
```

    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.
    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.
    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.
    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.
    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.
    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.



![png](output_8_1.png)


+ MAS vs Static


```python
labels = [
    'static',    # took 1.447 seconds
    '8kHz',      # took 166.501 seconds
]
filenames = [
    'eH_NOVEL_xband_loop10_static_inc40ns_zcw233_buildup.result',
    'eH_NOVEL_xband_loop10_8kHz_inc40ns_zcw233_buildup.result',
]
n = len(filenames)
colors = plt.cm.rainbow(np.linspace(0.05, 0.95, n))
markers = [
    'x', '+', 'o', 's', '^', 'D', 'p', 'h', 'v'
]

fig, axes = plt.subplots(2, 1, figsize=(10, 10), dpi=100)
for filename, color, m, label in zip(filenames, colors, markers, labels):
    filepath = os.path.join(data_dir, filename)
    df = get_data(filepath)
    axes[0].plot(df[0]*1e3, -df[1], color=color, label=label, linewidth=2)
    axes[1].plot(df[0]*1e3, -df[1], color=color, label=label, linewidth=1)
#     axes[2].plot(df[0]*1e3, -df[1], linestyle='solid', 
#                  color=color, label=label, markerfacecolor='none')

axes[1].set_xlim(1.52, 1.69)
axes[1].set_ylim(54.6, 63.2)
axes[1].set_xlabel('ms')
# axes[2].set_xlim(0.1701, 0.172)
# axes[2].set_ylim(11, 13.5)

axes[0].legend()
axes[1].legend(loc='best')
# axes[2].legend()
plt.savefig('assets/MASNOVEL/eH_xband_NOVEL_static_vs_MAS_options.ps')
plt.savefig('assets/MASNOVEL/eH_xband_NOVEL_static_vs_MAS_options.png')
plt.show()
```

    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.
    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.
    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.
    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.



![png](output_10_1.png)


#### Longer Time Period using ZCW233


```python
filenames = [
  'eH_NOVEL_xband_loop500_static_inc10us_zcw233_buildup.result',  # took 15.783 seconds
  'eH_NOVEL_xband_loop500_8kHz_inc40ns_zcw233_buildup.result',    # took 22548.549 seconds
]

labels = [
    'static',
    'MAS 8kHz',
]
n = len(filenames)
colors = plt.cm.rainbow(np.linspace(0.05, 0.95, n))
markers = [
    'x', '+', 'o', 's', '^', 'D', 'p', 'h', 'v'
]

fig, axes = plt.subplots(1, 1, figsize=(8, 6), dpi=100)
for filename, color, m, label in zip(filenames, colors, markers, labels):
    filepath = os.path.join(data_dir, filename)
    df = get_data(filepath)
    print(df.shape)
#     if df.shape[0] > 1e5:
#         df = df.iloc[::100, :]
#         print(df.shape)
    axes.plot(df[0], -df[1], color=color, label=label, linewidth=2)
axes.legend()
plt.savefig('assets/MASNOVEL/eH_NOVEL_xband_loop500_static_vs_mas_zcw233_buildup.ps')
plt.savefig('assets/MASNOVEL/eH_NOVEL_xband_loop500_static_vs_mas_zcw233_buildup.png')
plt.show()
```

    (9501, 2)
    (2128001, 2)


    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.
    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.



![png](output_12_2.png)



```python
# filename = 'eH_NOVEL_xband_loop400_8kHz_inc40ns_zcw34_buildup.result2'
# # took 1861.236 seconds

# filepath = os.path.join(data_dir, filename)
# df = get_data(filepath)
# print(df.shape)
# print(f'step size: {df[0][1] - df[0][0]}')
# print(f'step size: {df[0][2] - df[0][1]}')
# print(f'step size: {df[0][3] - df[0][2]}')
# fig, axes = plt.subplots(3, 1, figsize=(10, 8))
# axes[0].plot(df[0], df[1], 'bo-', markerfacecolor='none')
# axes[0].set_xlabel('Seconds')
# axes[0].set_ylabel('Enhancement')
# axes[0].set_title('NOVEL Powder Buildup with 8kHz MAS')

# n_pts = 40000
# axes[1].plot(df[0][:n_pts]*1e3, df[1][:n_pts], 'bo-', markerfacecolor='none')
# axes[1].set_xlabel('ms')
# axes[1].set_ylabel('Enhancement')

# n_pts = 50
# axes[2].plot(df[0][:n_pts]*1e6, df[1][:n_pts], 'bo-', markerfacecolor='none')
# axes[2].set_xlabel('$\mu$s')
# axes[2].set_ylabel('Enhancement')
# plt.subplots_adjust(hspace=0.3)
# plt.show()
```

## Field Profile


```python
filenames = [
    'eH_NOVEL_xband_loop60_static_inc40ns_zcw144_fp.result', # took 2.759 seconds
    'eH_NOVEL_xband_loop60_8kHz_inc40ns_zcw144_fp.result', # took 63811.16 seconds
    
#     'eH_NOVEL_xband_loop250_static_inc40ns_zcw233_fp.result', # took 4.868 seconds
#     'eH_NOVEL_xband_loop250_8kHz_inc40ns_zcw233_fp.result', # took 436135.134 seconds
]

labels = [
    'static',
    '8kHz',
]

n = len(filenames)
colors = plt.cm.rainbow(np.linspace(0.05, 0.95, n))

# filename = 'eH_NOVEL_xband_loop60_8kHz_inc40ns_zcw34_fp.result2'
# took 10681.54 seconds

fig = plt.figure(figsize=(8,6), dpi=200)
for filename, c, label in zip(filenames, colors, labels):
    filepath = os.path.join(data_dir, filename)
    df = get_data(filepath)
    plt.plot(df[0], df[1], marker='o', linestyle='solid', color=c, markerfacecolor='none', markersize=3, label=label)
plt.xlabel('Field (T)')
# plt.ylim(80, -400)
plt.ylim(40, -290)
plt.ylabel('Enhancement')
plt.legend()
plt.savefig('assets/MASNOVEL/eH_NOVEL_xband_loop60_static_vs_mas_zcw144_fp.ps')
plt.savefig('assets/MASNOVEL/eH_NOVEL_xband_loop60_static_vs_mas_zcw144_fp.png')
# plt.savefig('assets/MASNOVEL/eH_NOVEL_xband_loop250_static_vs_mas_zcw233_fp.ps')
# plt.savefig('assets/MASNOVEL/eH_NOVEL_xband_loop250_static_vs_mas_zcw233_fp.png')
plt.show()
```

    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.
    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.



![png](output_15_1.png)



```python

```


```python

```
