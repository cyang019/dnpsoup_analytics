```python
import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
```


```python
os.getcwd()
```




    '/home/chen/coding/projects/dnpsoup/dnpsoup_analytics'




```python
data_dir = 'results/CW_CrossEffect/'
```


```python
def get_data(filename):
    df = pd.read_csv(filename, skiprows=2, header=None)
    return df
```


```python
%matplotlib inline
```


```python

```

#### BuildUps (Mixing Time)

+ Single Crystal


```python
datadir = 'outputs/CE/xtal_buildups'
labels = [
    'a0b0g0', # took 7.976 14.175 seconds
    'a0b15g0', # took 7.839 14.492 seconds
    'a0b30g0', # took 7.773 14.613 seconds
    'a0b45g0', # took 7.819 14.242 seconds
    'a0b60g0', # took 7.823 14.411 seconds
    'a0b75g0', # took 7.760 14.498 seconds
    'a0b90g0', # took 7.785 14.903 seconds
    'a0b105g0', # took 7.771 14.584 seconds
    'a0b120g0', # took 7.795 14.371 seconds
    'a0b135g0', # took 7.772 14.408 seconds
    'a0b150g0', # took 7.801 14.501 seconds
    'a0b165g0', # took 7.771 14.310 seconds
    'a0b180g0', # took 7.786 14.255 seconds
]

filenames = [
    f'eeH_test_coord_e2_a0b60g150_H_xn2y2z1_p1ms_{label}_9p393T_inc40ns_buildup.result' for label in labels
    
]
n = len(filenames)
colors = plt.cm.rainbow(np.linspace(0.05, 0.95, n//2+1))

fig, axes = plt.subplots(2, 1, figsize=(10, 10), dpi=100)
for filename, color, label in zip(filenames[:7], colors, labels[:7]):
    filepath = os.path.join(datadir, filename)
    df = get_data(filepath)
#     df = df.iloc[:20000, :]
    print(f'[{label}]: {df.shape[0]} data points')
    axes[0].plot(df[0]*1e3, df[1], color=color, label=label, linewidth=2)
for filename, color, label in zip(filenames[6:], colors[::-1], labels[6:]):
    filepath = os.path.join(datadir, filename)
    df = get_data(filepath)
#     df = df.iloc[:20000, :]
    print(f'[{label}]: {df.shape[0]} data points')
    axes[1].plot(df[0]*1e3, df[1], color=color, label=label, linewidth=2)

axes[0].legend()
axes[1].legend()
axes[1].set_xlabel('ms')
plt.show()
```

    [a0b0g0]: 25001 data points
    [a0b15g0]: 25001 data points
    [a0b30g0]: 25001 data points
    [a0b45g0]: 25001 data points
    [a0b60g0]: 25001 data points
    [a0b75g0]: 25001 data points
    [a0b90g0]: 25001 data points
    [a0b90g0]: 25001 data points
    [a0b105g0]: 25001 data points
    [a0b120g0]: 25001 data points
    [a0b135g0]: 25001 data points
    [a0b150g0]: 25001 data points
    [a0b165g0]: 25001 data points
    [a0b180g0]: 25001 data points



![png](output_8_1.png)


+ Enhancement


```python
datadir = 'outputs/CE/xtal_buildup_enhancement'
labels = [
    'a0b0g0', # took 14.284 secs ?26.544 seconds
    'a0b15g0', # took 14.300 secs ? 26.472 seconds
    'a0b30g0', # took 14.329 secs ? 26.412 seconds
    'a0b45g0', # took 14.425 secs ? 26.536 seconds
    'a0b60g0', # took 14.474 secs ? 26.592 seconds
    'a0b75g0', # took 14.385 secs ? 26.572 seconds
    'a0b90g0', # took 14.273 secs ? 27.289 seconds
    'a0b105g0', # took 14.219 secs ? 26.411 seconds
    'a0b120g0', # took 14.136 secs ? 27.383 seconds
    'a0b135g0', # took 14.245 secs ? 26.372 seconds
    'a0b150g0', # took 14.393 secs ? 27.159 seconds
    'a0b165g0', # took 14.343 secs ? 26.869 seconds
    'a0b180g0', # took 14.557 secs ? 26.348 seconds
]

filenames = [
    f'eeH_test_coord_e2_a0b60g150_H_xn2y2z1_p1ms_{label}_9p393T_inc40ns_buildup_enhancement.result' for label in labels
    
]
n = len(filenames)
colors = plt.cm.rainbow(np.linspace(0.05, 0.95, n//2+1))

fig, axes = plt.subplots(2, 1, figsize=(10, 10), dpi=100)
for filename, color, label in zip(filenames[:7], colors, labels[:7]):
    filepath = os.path.join(datadir, filename)
    df = get_data(filepath)
#     df = df.iloc[:20000, :]
    print(f'[{label}]: {df.shape[0]} data points')
    axes[0].plot(df[0]*1e3, df[1], color=color, label=label, linewidth=2)
for filename, color, label in zip(filenames[6:], colors[::-1], labels[6:]):
    filepath = os.path.join(datadir, filename)
    df = get_data(filepath)
#     df = df.iloc[:20000, :]
    print(f'[{label}]: {df.shape[0]} data points')
    axes[1].plot(df[0]*1e3, df[1], color=color, label=label, linewidth=2)

axes[0].legend()
axes[1].legend()
axes[1].set_xlabel('ms')
plt.savefig('eeH_e2_a0b60g150_H_xn2y2z1_p1ms_euler_angles_9p393T_inc40ns_buildup_enhancement.ps')
plt.show()
```

    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.
    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.


    [a0b0g0]: 25001 data points
    [a0b15g0]: 25001 data points
    [a0b30g0]: 25001 data points
    [a0b45g0]: 25001 data points
    [a0b60g0]: 25001 data points
    [a0b75g0]: 25001 data points
    [a0b90g0]: 25001 data points
    [a0b90g0]: 25001 data points
    [a0b105g0]: 25001 data points
    [a0b120g0]: 25001 data points
    [a0b135g0]: 25001 data points
    [a0b150g0]: 25001 data points
    [a0b165g0]: 25001 data points
    [a0b180g0]: 25001 data points


    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.
    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.



![png](output_10_3.png)


+ Increment


```python
datadir = 'outputs/CE/xtal_buildup_enhancement_inc'
labels = [
    '1ns', # took 1209.152 seconds
    '10ns', # took 48.450 seconds
    '40ns', # took 13.950 seconds
    '50ns', # took 12.249 seconds
    '60ns', # took 10.636 seconds
#     '65ns', # took 9.667 seconds
#     '69ns', # took 10.957 seconds
#     '70ns', # took 10.584 seconds   *** this one has issue ***
#     '71ns', # took 10.670 seconds
    '80ns', # took 9.152 seconds
    '100ns', # took 7.365 seconds
]

filenames = [
    f'eeH_test_coord_e2_a0b60g150_H_xn2y2z1_p1ms_a0b75g0_9p393T_inc{label}_buildup_enhancement.result' for label in labels
    
]
n = len(filenames)
colors = plt.cm.rainbow(np.linspace(0.05, 0.95, n))

fig, axes = plt.subplots(2, 1, figsize=(10, 10), dpi=100)
for filename, color, label in zip(filenames, colors, labels):
    filepath = os.path.join(datadir, filename)
    df = get_data(filepath)
#     df = df.iloc[:20000, :]
    print(f'[{label}]: {df.shape[0]} data points')
    axes[0].plot(df[0]*1e3, df[1], color=color, label=label, linewidth=2)
    axes[1].plot(df[0]*1e3, df[1], color=color, label=label, linewidth=2)

axes[0].legend()
axes[1].legend()
axes[1].set_xlim(0.6, 1.0)
axes[1].set_ylim(115, 150)
axes[1].set_xlabel('Mixing Time (ms)')
plt.savefig('eeH_e2_a0b60g150_H_xn2y2z1_p1ms_a0b75g0_9p393T_incs_buildup_enhancement.ps')
plt.show()
```

    [1ns]: 1000001 data points
    [10ns]: 100001 data points
    [40ns]: 25001 data points
    [50ns]: 20001 data points
    [60ns]: 16668 data points
    [80ns]: 12501 data points
    [100ns]: 10001 data points


    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.
    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.
    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.
    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.



![png](output_12_2.png)


+ Powder Options


```python
datadir = 'outputs/CE/buildup_powder_options'
labels = [
    'zcw21',  # took 73.809 seconds
    'zcw34',  # took 103.205 seconds
    'zcw55',  # took 161.981 seconds
    'zcw89',  # took 261.834 seconds
    'zcw144', # took 423.259 seconds
]

filenames = [
    f'eeH_e2_a0b60g150_H_xn2y2z1_p1ms_{label}_9p393T_inc40ns_buildup_enhancement.result' for label in labels
    
]
n = len(filenames)
colors = plt.cm.rainbow(np.linspace(0.05, 0.95, n))

fig, axes = plt.subplots(1, 1, figsize=(10, 6), dpi=100)
for filename, color, label in zip(filenames, colors, labels):
    filepath = os.path.join(datadir, filename)
    df = get_data(filepath)
#     df = df.iloc[:20000, :]
    print(f'[{label}]: {df.shape[0]} data points')
    axes.plot(df[0]*1e3, df[1], color=color, label=label, linewidth=2)
    
axes.legend()
axes.set_xlabel('ms')
plt.savefig('eeH_e2_a0b60g150_H_xn2y2z1_p1ms_powder_options_9p393T_inc40s_buildup_enhancement.ps')
plt.show()
```

    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.
    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.


    [zcw21]: 25001 data points
    [zcw34]: 25001 data points
    [zcw55]: 25001 data points
    [zcw89]: 25001 data points
    [zcw144]: 25001 data points



![png](output_14_2.png)



```python
from scipy.stats import linregress
```


```python
time_taken = [73.809, 103.205, 161.981, 261.834, 423.259]
powders = [21, 34, 55, 89, 144]

slope, intercept, r_value, p_value, std_err = linregress(powders, time_taken)

fig = plt.figure(figsize=(8, 6), dpi=100)
plt.plot(powders, time_taken, 'bo', markerfacecolor='none')
plt.plot(powders, slope * np.array(powders) + intercept, 'b-', label=f'r: {r_value:.4f}  p: {p_value:.4e}')
plt.xlabel('Powder Count (a.u.)')
plt.ylabel('Time Taken (seconds)')
plt.legend()
plt.show()
```


![png](output_16_0.png)


#### Eigen Values


```python

```


```python

```


```python

```

#### Field Profiles
+ powder schemes
+ e-e dipole (CE) vs no dipole (SE)


```python
datadir = 'outputs/CE'
labels = [
    'CE', # 21062.274 seconds
#     'SE',
]

filenames = [
    'eeH_e2_a0b60g150_H_xn2y2z1_p1ms_zcw144_inc40ns_fp_9p36T_to_9p41T.result'
]

n = len(filenames)
colors = plt.cm.rainbow(np.linspace(0.1, 0.9, n))
fig, axes = plt.subplots(1, 1, figsize=(10, 6), dpi=100)
for filename, color, label in zip(filenames, colors, labels):
    filepath = os.path.join(datadir, filename)
    df = get_data(filepath)
    print(f'[{label}]: {df.shape[0]} data points')
    axes.plot(df[0], df[1], color=color, label=label, linewidth=2)
    
axes.legend()
axes.set_xlabel('Field (T)')
plt.show()
```

    [CE]: 50 data points



![png](output_22_1.png)



```python

```


```python

```
