```python
import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.pylab as pl
```


```python
%matplotlib inline
```


```python
os.getcwd()
```




    '/home/chen/coding/projects/dnpsoup/dnpsoup_analytics'




```python
os.listdir('outputs/NOVEL/')
```




    ['eH_NOVEL_xband_loop6k_zcw144_fp.result',
     'eH_NOVEL_xband_loop6k_zcw2584_fp.result',
     'eH_NOVEL_xband_scan_d.result',
     'eH_NOVEL_xband_loop6k_scan_gammaB1.result',
     'eH_NOVEL_xband_scan_pmix.result',
     'eH_NOVEL_xband_scan_loop.result']




```python
datadir = 'outputs/NOVEL'
```


```python
if not os.path.exists('assets/NOVEL'):
    os.makedirs('assets/NOVEL')
```

### Scan Mixing Time


```python
# took 3.306 seconds.zcw144
filename = 'eH_NOVEL_xband_scan_pmix.result'
filepath = os.path.join(datadir, filename)
df = pd.read_csv(filepath, skiprows=2, header=None)
fig = plt.figure(figsize=(8, 6), dpi=300)
plt.plot(df[0], -df[1], 'b-o', markerfacecolor='none')
plt.xlabel('Mixing Time (ns)')
plt.ylabel('Enhancement (A.U.)')
plt.savefig('assets/NOVEL/eH_NOVEL_xband_scan_pmix.ps')
plt.savefig('assets/NOVEL/eH_NOVEL_xband_scan_pmix.png')
plt.show()
```


![png](output_7_0.png)


### Scan Delay


```python
# took 9.140 seconds
filename = 'eH_NOVEL_xband_scan_d.result'
filepath = os.path.join(datadir, filename)
df = pd.read_csv(filepath, skiprows=2, header=None)
fig = plt.figure(figsize=(8, 6), dpi=300)
plt.plot(df[0]/1e3, -df[1], 'b-o', markerfacecolor='none')
plt.xlabel('Delay ($\mu$s)')
plt.ylabel('Enhancement (A.U.)')
plt.savefig('assets/NOVEL/eH_NOVEL_xband_scan_d.ps')
plt.savefig('assets/NOVEL/eH_NOVEL_xband_scan_d.png')
plt.show()
```


![png](output_9_0.png)


### Scan Repetition Count


```python
# took 14.95 seconds
filename = 'eH_NOVEL_xband_scan_loop.result'
filepath = os.path.join(datadir, filename)
df = pd.read_csv(filepath, skiprows=2, header=None)
fig = plt.figure(figsize=(8, 4), dpi=300)
plt.plot(df[0], -df[1], 'b-o', markerfacecolor='none')
plt.xlabel('Loop Count (A.U.)')
plt.ylabel('Enhancement (A.U.)')
plt.savefig('assets/NOVEL/eH_NOVEL_xband_scan_loop.ps')
plt.savefig('assets/NOVEL/eH_NOVEL_xband_scan_loop.png')
plt.show()
```


![png](output_11_0.png)


### Scan $\omega_{1s}$


```python
# took 3.274 seconds
filename = 'eH_NOVEL_xband_loop6k_scan_gammaB1.result'
filepath = os.path.join(datadir, filename)
df = pd.read_csv(filepath, skiprows=2, header=None)
fig = plt.figure(figsize=(8, 6), dpi=300)
plt.plot(df[0]/1e6, -df[1], 'b-o', markerfacecolor='none')
plt.xlabel('$\omega_{1s}$ (MHz)')
plt.ylabel('Enhancement (A.U.)')
plt.savefig('assets/NOVEL/eH_NOVEL_xband_scan_gammaB1.ps')
plt.savefig('assets/NOVEL/eH_NOVEL_xband_scan_gammaB1.png')
plt.show()
```


![png](output_13_0.png)


### Field Profile


```python
# took 5.83 seconds
filename = 'eH_NOVEL_xband_loop6k_zcw144_fp.result'
filepath = os.path.join(datadir, filename)
df = pd.read_csv(filepath, skiprows=2, header=None)
fig = plt.figure(figsize=(8, 6), dpi=300)
plt.plot(df[0], df[1], 'b-o', markerfacecolor='none')
plt.xlabel('Magnetic Field (T)')
plt.ylabel('Enhancement (A.U.)')
plt.savefig('assets/NOVEL/eH_NOVEL_xband_fp.ps')
plt.savefig('assets/NOVEL/eH_NOVEL_xband_fp.png')
plt.show()
```


![png](output_15_0.png)



```python
# took 89.384 seconds
filename = 'eH_NOVEL_xband_loop6k_zcw2584_fp.result'
filepath = os.path.join(datadir, filename)
df = pd.read_csv(filepath, skiprows=2, header=None)
fig = plt.figure()
plt.plot(df[0], df[1], 'b-+')
plt.xlabel('Magnetic Field (T)')
plt.ylabel('Enhancement (A.U.)')
plt.show()
```


![png](output_16_0.png)



```python

```
