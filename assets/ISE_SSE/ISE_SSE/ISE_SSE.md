```python
import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.pylab as pl
```


```python
%matplotlib inline
```


```python
os.getcwd()
```




    '/home/chen/coding/projects/dnpsoup/dnpsoup_analytics'




```python
data_dir = 'outputs/ISE_SSE/'
if not os.path.exists(data_dir):
    os.makedirs(data_dir)
fig_dir = 'assets/ISE_SSE/'
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)
```

### Scan Loop


```python
# took 542.432 seconds
filename = 'eH_gtensor_trityl_a0b40g0_wband_chirp_300MHz_oct89_scan1d_loop.result'
filepath = os.path.join(data_dir, filename)
df = pd.read_csv(filepath, skiprows=2, header=None)
fig = plt.figure()
plt.plot(df[0], df[1], 'b-o', markerfacecolor='none')
plt.xlabel('Count of Repetition (A.U.)')
plt.ylabel('Enhancement (A.U.)')
plt.show()
```


![png](output_5_0.png)


### Field Profile


```python
# took 1208.394 seconds
filename1 = 'eH_gtensor_trityl_a0b40g0_wband_loop4k_chirp_300MHz_oct89_em_fp_step0p01.result'
# took 1215.464 seconds
filename2 = 'eH_gtensor_trityl_a0b40g0_wband_loop4k_chirp_neg300MHz_oct89_em_fp_step0p01.result'
filepath1 = os.path.join(data_dir, filename1)
filepath2 = os.path.join(data_dir, filename2)
df1 = pd.read_csv(filepath1, skiprows=2, header=None)
df2 = pd.read_csv(filepath2, skiprows=2, header=None)
fig = plt.figure(figsize=(8, 6), dpi=300)
plt.plot(df1[0]/1e9, df1[1], 'r-o', markerfacecolor='none', label='300MHz')
plt.plot(df2[0]/1e9, df2[1], 'b-s', markerfacecolor='none', label='-300MHz')
plt.xlabel('Gyrotron Frequency (GHz)')
plt.ylabel('Enhancement (A.U.)')
plt.legend()
figname = 'ISE_SSE_wband_fp.ps'
figpath = os.path.join(fig_dir, figname)
plt.savefig(figpath)
plt.show()
```

    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.
    The PostScript backend does not support transparency; partially transparent artists will be rendered opaque.



![png](output_7_1.png)



```python

```
