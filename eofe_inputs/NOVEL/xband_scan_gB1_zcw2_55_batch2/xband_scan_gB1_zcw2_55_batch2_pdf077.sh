#!/bin/bash
#SBATCH -p sched_mit_psfc
#SBATCH --ntasks=118
#SBATCH --cpus-per-task=1
#SBATCH -J CE
#SBATCH --export=OMP_NUM_THREADS,ALL
module rm gcc/4.8.4
module add gcc/8.3.0
module load gcc/8.3.0

# launch the code
export OMP_NUM_THREAD=1
srun --multi-prog xband_scan_gB1_zcw2_55_batch2_pdf077.config
