#!/bin/bash
#SBATCH -p sched_mit_psfc
#SBATCH --ntasks=52
#SBATCH --cpus-per-task=1
#SBATCH -J CE
#SBATCH --export=OMP_NUM_THREADS,ALL
module rm gcc/4.8.4
module add gcc/8.3.0
module load gcc/8.3.0

# launch the code
export OMP_NUM_THREAD=1
srun --multi-prog eeH_d3_a10bn70g0_t1e0p5ms_t2e0p1us_t1n2p0s_t2n1p0ms_0p4MHz_ref.config
