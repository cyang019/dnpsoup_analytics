#!/bin/bash
#SBATCH -p sched_mit_psfc
#SBATCH --ntasks=52
#SBATCH --cpus-per-task=1
#SBATCH -J CE
#SBATCH --export=OMP_NUM_THREADS,ALL
module rm gcc/4.8.4
module add gcc/8.3.0
module load gcc/8.3.0

# launch the code
export OMP_NUM_THREAD=1
srun --multi-prog eeH_a0b105g0_t1e0p6ms_t2e3us_t1n2s_t2n1ms_euler204_euler_131.config
