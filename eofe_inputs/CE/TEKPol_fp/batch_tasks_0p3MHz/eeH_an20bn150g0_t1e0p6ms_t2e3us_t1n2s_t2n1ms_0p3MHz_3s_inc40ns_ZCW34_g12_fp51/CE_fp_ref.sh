#!/bin/bash
#SBATCH -p sched_mit_psfc
#SBATCH --ntasks=52
#SBATCH --cpus-per-task=1
#SBATCH -J CE
#SBATCH --export=OMP_NUM_THREADS,ALL
module rm gcc/4.8.4
module add gcc/8.3.0
module load gcc/8.3.0

# launch the code
export OMP_NUM_THREAD=1
srun --multi-prog eeH_an20bn150g0_t1e0p6ms_t2e3us_t1n2s_t2n1ms_0p3MHz_3s_inc40ns_ZCW34_g12_fp51_ref.config
