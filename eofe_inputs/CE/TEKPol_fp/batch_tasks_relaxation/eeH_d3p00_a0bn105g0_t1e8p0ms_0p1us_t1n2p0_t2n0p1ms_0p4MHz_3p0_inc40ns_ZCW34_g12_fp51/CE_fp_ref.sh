#!/bin/bash
#SBATCH -p sched_mit_psfc
#SBATCH --ntasks=52
#SBATCH --cpus-per-task=1
#SBATCH -J CE
#SBATCH --export=OMP_NUM_THREADS,ALL
module rm gcc/4.8.4
module add gcc/8.3.0
module load gcc/8.3.0

# launch the code
export OMP_NUM_THREAD=1
srun --multi-prog eeH_d3p00_a0bn105g0_t1e8p0ms_0p1us_t1n2p0_t2n0p1ms_0p4MHz_3p0_inc40ns_ZCW34_g12_fp51_ref.config
